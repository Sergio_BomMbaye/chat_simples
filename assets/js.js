var socket;
var id_dest;
var destinario;
$(document).ready(function(){
    socket = io.connect("http://10.1.1.2:8080");
    var ready = false;
    var nome;

    //listar os pessoa on line
   socket.on('nome',function (lists) {
        for(var i = 0; i <lists.length;i++){
           $('.user').append('<ul class="info" id="nome" >'+lists[i]+'</ul>');
       };
   });

    //PARTE DOS ROOMS
    //Listener , whenever the server emits 'updaterooms' ,this updates the room the client is in
    socket.on('updaterooms',function (rooms,current_room) {
        $("#rooms").empty();
        $.each(rooms,function (key,value) {
            console.log(rooms+ " : "+value+ " : "+key +" : "+current_room);
            if(value == current_room){
                $("#rooms").append('<div ><a id="select_dest">'+value+'</a></div>');
            }else {
                $("#rooms").append('<div ><button href="#" id="bt_rom" onclick="switchRoom(\''+value+'\')">'+value+'</button></div>');
            }
        });
    });



    //funcao quando colco  seu nome no impute e clica entre
    $("#submit").submit(function(e) {
		e.preventDefault();
		$("#nick").fadeOut();
		$("#onli").fadeOut();
		var name = $("#nickname").val();
		var select = $("#selecao").val();
		var time = new Date();
		$("#name").html(name);
		$("#time").html('Hora Logado: ' + time.getHours() + ':' + time.getMinutes());
		ready = true;
		socket.emit('join', name );
		$("#chat").fadeIn();
	});


    //quando cara escrevo mesage e aperta entre
	$("#textarea").keypress(function(e){
        if(e.which == 13) {
            $("#if").fadeOut();
            $("#digit").fadeOut();
            $("#no_dig").fadeOut();
        	var text = $("#textarea").val();
        	var time = new Date();
            console.log(" mostra : "+id_dest);
            if(destinario.isU) {
                //socket.emit("send", text,id_dest);
        	    $(".chat").append('<li class="self"><div class="msg"><span>' + $("#nickname").val() + ':</span><p>' + text + '</p><time>' + time.getHours() + ':' + time.getMinutes() + '</time></div></li>');
        	    $("#textarea").val('');
        	    $("#meb").fadeIn();
        	    destinario = "";
            }else{
                alert("seleciona alguem para se conversa");
            }
        }
    });

    //funcao avisar quem conecto
    socket.on("update", function(result,msg) {
    	$("#if").fadeIn();
        $("#meb").fadeIn();
        console.log(result.length);
    	if (ready) {
    		$('.conect').append('<ul class="info">' + msg + '</ul>');
            if(result.length){
              for (var i =0;i<result.length;i++) {

    		    $('.member_list').append('' +
                    '<ul id="yaw" aux="'+i+'" data="'+result[i]["id"]+'" class="list-unstyled">' +
                    '<li class="left clearfix">' +
                    '<span class="chat-img pull-left">' +
                    '<img src="https://imagem.natelinha.uol.com.br/grande/sheronmenezes_c8eb12036156d91257760b4ccd9b43fd6ca594c5.jpeg" ' +
                    'style="width: 30px;height: 30px" alt="User Avatar" class="img-circle">' +
                    '</span> ' +
                    '<div class="chat-body clearfix"> ' +
                    '<div class="header_sec" id="di_mes">' +
                    '<strong class="primary-font" id="nom_cli" >'+result[i]["id"]+'</strong> ' +
                    '<strong class="pull-right" id="hor_mes" >'+result[i]["hora"]+"AM"+'</strong> ' +
                    '</div>' +
                    '<div class="contact_sec">' +
                    '<strong class="primary-font" id="mes_cli">'+result[i]["message"]+'</strong> ' +
                    '<span class="badge pull-right">2</span>' +
                    '</div>' +
                    '</div>' +
                    '</li> ' +
                    '</ul>');
              }
            }
         }
    });


//Funcao digitar avisa
    $("#textarea").on('keypress',function (key) {
        var t ="";
        if($("#textarea").keyup()){
            t = "digit";
            socket.emit('digitando',t);
            console.log("mandei");
            if(key.which == 13){
                t = "entre";
                socket.emit('digitando',t);
            }
        };
    });
    socket.on("digito",function (id,signe) {
        if(signe === "digit" ) {
           console.log("client recebi : " + id);
            $("#no_dig").fadeIn();
            $("#no_dig").html(id);
            $("#digit").fadeIn();
        }else if(signe === "entre"){
            console.log("client recebi : " + id);
            $("#no_dig").fadeOut();
            $("#digit").fadeOut();
        }
    });


    //listagem messagem
   $('.member_list').on("click","ul#yaw",function () {
       console.log('oi'+$(this).attr("aux"));
       var th = $(this).attr("data");
       console.log(th);
      socket.emit('selection',th);
       socket.on('selec',function (result) {
           if(result.length) {
               for (var i = 0; i < result.length; i++) {
                   $(".chat").append('<li class="other"><div class="msg">' +
                       '<span>'+result[i]["id"]+':</span>' +
                       '<p>'+result[i]["message"]+'</p>' +
                       '<time>'+result[i]["hora"]+ '</time>' +
                       '</div>' +
                       '</li>');
               }
           }
       })
    });

    //funcao formula de aparecer messagem
    socket.on("chat", function(client,msg) {
    	if (ready) {
	    	var time = new Date();
	    	$(".chat").append('<li class="other"><div class="msg"><span>' + client + ':</span><p>' + msg + '</p><time>' + time.getHours() + ':' + time.getMinutes() + '</time></div></li>');
            $("#nom_cli").html(client);
            $("#mes_cli").html(msg);
            $("#hor_mes").html(time.getHours() + ':' + time.getMinutes())
    	}
    });
});

