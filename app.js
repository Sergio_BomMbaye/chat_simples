const express = require('express');
const app =  express();
const http = require('http').Server(app);
const io = require('socket.io')(http);
var mongodb = require('mongodb');
const isUndefined = require("is-undefined");
var lists = [];
var clients = {};
var rooms = ['Daniel','Serigne','Felipe'];
var keypress = require('keypress');




//definie nome do meu css
app.use(express.static('assets'));

//esperando requisicao
app.get('/', function(req, res){
  //res.send('server is running');
    res.sendFile(__dirname +'/index.html')
});


//quando algue conecto
io.sockets.on("connection", function (client) {

    //emit nome conectados console.log('listes nomes : '+lists )
    client.emit('nome',lists);


    // se cliente conecto com publico
    client.on("join", function(name){
        console.log("Joined: " + name);
        lists.push(name);
        clients[client.id] = name;
        client.room = 'room1';
        client.join('room1');
        //selecionar s messagem
        var MongoClient = mongodb.MongoClient;
        var url = 'mongodb://localhost:27017/db_message';
        MongoClient.connect(url,function (err,db) {
            if(err){
                console.log('Unable to connect to the server',err);
            }else{
                console.log("Connection Established");
                var collection = db.collection('message');
                collection.find({id :{$eq :clients[client.id]}}).toArray(function (err,result) {
                    if(err){
                        console.log(err);

                        //res.send(err);
                    }else if(result.length){
                        var tp =0;
                        //console.log(result);
                        client.emit("update",result, ""+name+" Voce esta conectado no servidor");
                        //client.broadcast.to('room1').emit("update", tp,""+name + " Entro  na Sala  ");
                        client.emit('updaterooms',rooms,'inicio');
                        console.log('mandei');
                    }else  {
                        console.log('No documents found');
                    }
                    db.close();
                });
            }
        });
    });

    //pedid selection message
    client.on('selection',function (id) {
        var MongoClient = mongodb.MongoClient;
        var url = 'mongodb://localhost:27017/db_message';
        MongoClient.connect(url,function (err,db) {
            if(err){
                console.log('Unable to connect to the server',err);
            }else{
                console.log("Connection Established");
                var collection = db.collection('message');
                collection.find({id :{$eq :id}}).toArray(function (err,result) {
                    if(err){
                        console.log(err);
                    }else if(result.length){
                        console.log(result);
                        client.emit('selec',result);
                        console.log('mandei_resultado');
                    }else  {
                        console.log('No documents found');
                    }
                    db.close();
                });
            }
        });
    });


    // se cliente conecta com privado
    client.on('private',function (nome,name) {
        lists.push(name);
        clients[client.id] = nome;
        console.log("Joined private: " + nome);
        client.emit("update","" + name +"   Voce esta conectando no servidor com  "+nome  )
    });

    //se cliente esta digitando
    client.on("digitando",function (signe) {
        client.broadcast.emit('digito',clients[client.id],signe);
    });

    client.on("press",function () {
        console.log('recebiii : ' );
        client.broadcast.emit("sim",clients[client.id]);
    });

    //cliente mandar messagem
    client.on("send", function(msg){
        //guarda as  messagem

        var MongoClient = mongodb.MongoClient;
        var url = 'mongodb://localhost:27017/db_message';
        MongoClient.connect(url,function (err,db) {
            if(err){
                console.log("Unable to connect to server ",err);
            }else {
                var time = new Date();
                console.log('Connected to server');
                var collection = db.collection('message');
                var dados = {message : msg
                    ,id: clients[client.id]
                    ,hora: time.getHours()+":"+time.getMinutes()
                    ,id_dest: clients[client.id] /*nome destinario */};
                collection.insert([dados],function (err,result) {
                    if(err){
                        console.log('not inserted');
                        console.log(err);
                    }else{
                        console.log('inserted');
                        console.log("Message: " + msg);
                        //io.sockets.in(client.room).broadcast
                        //client.broadcast.to('room1').emit("chat", clients[client.id], msg);
                        //io.client.to("room-"+roomno).emit('connectToRoom', "You are in room no. "+roomno);
                        client.broadcast.to(client.room).emit("chat", clients[client.id], msg);
                    }
                    db.close();
                });
            }
        });
    });


    //gerencia os rooms
    client.on('switchRoom',function (newroom,name) {
        var tp = 0;
        client.leave(client.room);
        client.join(newroom);
        client.emit("update",tp, ""+name+" Voce esta conectado no servidor  "+newroom);
        client.broadcast.to(client.room).emit("update", tp,""+name + " Saiu  na Sala  ");
        client.room = newroom;
        client.broadcast.to(newroom).emit('update',tp,""+name+" Entro na Sala");
        client.emit('updaterooms',rooms,newroom);
    });

    //cliente disconectado
    client.on("disconnect", function(){
        if(isUndefined(clients[client.id])){
            console.log("Pagina Atualizado")
        }else {
            var tp =0;
            console.log(clients[client.id])
            console.log("Disconnect");
            io.emit("update", tp+""+clients[client.id] + " Foi Deconectado");
            delete clients[client.id];
            lists.pop(clients[client.id]);
        }
    });
});


http.listen(8080, function(){
  console.log('listening on port 8080');
});
